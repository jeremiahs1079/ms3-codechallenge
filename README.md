##Challenge Summary
A customer wants an application that can will consume a csv file and create an in memory database. The app should parse
the data in the csv and remove the records that have missing data, which will be written to a new csv titled `bad-data-<timestamp>.csv`.
The remaining records will be inserted into an in memory database matching the header columns of the csv to the columns in the database.

##Challenge Rules
Customer X just informed us that we need to churn out a code enhancement ASAP for a new project.  Here is what they need:

1. We need a Java application that will consume a CSV file, parse the data and insert to a SQLite In-Memory database.  
a. Table X has 10 columns A, B, C, D, E, F, G, H, I, J which correspond with the CSV file column header names.
b. Include all DDL in submitted repository
c. Create your own SQLite DB

2. The data sets can be extremely large so be sure the processing is optimized with efficiency in mind.  

3. Each record needs to be verified to contain the right number of data elements to match the columns.  
a. Records that do not match the column count must be written to the bad-data-<timestamp>.csv file
b. Elements with commas will be double quoted

4. At the end of the process write statistics to a log file
a. # of records received
b. # of records successful
c. # of records failed

Rules and guidelines:
1) Feel free to use any online resources
2) Utilizing existing tools like Maven and open source libraries is encouraged.
3) A finished solution is great but if you do not get it all completed, that is ok - we will evaluate based on approach
4) It is required that you provide a README detailing the challenge. Your readme should include instructions to run your code, and a brief paragraph describing the approach you took to solve the challenge

##Assumptions for Program Execution
1. The csv file being processed will always be named `ms3Interview.csv`
2. The csv file will be located in the resource directory of the project.
3. Gradle will be used to build and run the project


##Process for Solving Problem
To start the process I decided to use a gradle build system to manage the build process and running the application. This allows
for easy project building and managing.

The entry point of the application is in the MyApp class but the bulk of the work is done in the CSVProcessor class. This class
handles all the processing of the CSV Records obtained form the apache commons csv library. It first separates the records into
two lists, one containing all the records that are missing data in the columns and one that has the records that are complete. 
Each list is then processed, the records missing data are written to a bad data csv file and the records that are complete are written
to an in memory database.

After the program process the csv records the statistics are written to a log file `stats_log.txt` via the static method `writeStatistics` in the 
StatWriter class.

I chose to break this process into three classes to facilitate class cohesion. I originally wanted to create a database access layer but decided to just have the
CSVProcessor class access the database directly. The trade off bing decreased class cohesion for decrease coupling between the potential database access layer and the csv processor.

##Run and build the project
From the project root directory run:
####Linux or Mac
 `gradlew build` to build the project
 `gradlew run` to run the application
 
####Windows
 `gradlew.bat build` to build the project
 `gradlew.bat run` to run the application

##Open Source Libraries Used
Apache Commons CSV - for opening the csv and processing the lines into CSV Records

Xerial SQLLite JDBC - for database access

##Issues and Concerns
I looked at several resources for using an in memory data base and according to the resources I found the way to do this was
to add `::memory:` to the JDBC connection string. This did seem to work but i did not know how to test weather the database was functioning properly.
Therefore I decided to create a sqllite db file called mydb.db in the db directory of the resources folder that already had the table built. This allowed me to test
that the database was being populated with the records as expected. There is a commented method in the CSVProcessor class that allows this functionality to be enabled.
