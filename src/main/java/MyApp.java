import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class MyApp {

    public static void main(String args[])  {

        try {

            CSVProcessor processor = new CSVProcessor();
            InputStream in = MyApp.class.getResourceAsStream("ms3Interview.csv");
            InputStreamReader data = new InputStreamReader(in);
            List<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(data).getRecords();

            processor.processCSVRecords(records);

        }
        catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
        }

    }
}
