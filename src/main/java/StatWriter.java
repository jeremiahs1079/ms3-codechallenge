import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class StatWriter {

    /**
     * Method to print the processing statistics of the records in the CSV file provided
     * @param received the number of records processed
     * @param successful the number of records that where successfully processed
     * @param failed the number of records that failed the record column count
     */
    public static void writeStatistics(long received, long successful, long failed) {


        try {

            FileWriter fileWriter = new FileWriter("stats_log.txt");
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.printf("# of records received: %d\n", received);
            printWriter.printf("# of records successful: %d\n", successful);
            printWriter.printf("# of records failed: %d\n", failed);
            printWriter.close();

        } catch (IOException ioException) {

        }
    }





}
