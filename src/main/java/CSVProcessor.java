import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CSVProcessor {

    private final String COLUMN_A = "A";
    private final String COLUMN_B = "B";
    private final String COLUMN_C = "C";
    private final String COLUMN_D = "D";
    private final String COLUMN_E = "E";
    private final String COLUMN_F = "F";
    private final String COLUMN_G = "G";
    private final String COLUMN_H = "H";
    private final String COLUMN_I = "I";
    private final String COLUMN_J = "J";

    private Connection conn;
    private PreparedStatement batchStatment;
    private Statement stmt;

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm");


    /**
     * Method to process all the csv records in a CSVRecord list and separate the records with missing data.
     * @param records a List of CSVRecord to process
     * @return an ArrayList of records that
     */
    public void processCSVRecords(List<CSVRecord> records) {

        //declare empty lists to store the passed and failed records
        ArrayList<CSVRecord> failedRecords = new ArrayList<>();
        ArrayList<CSVRecord> passedRecords = new ArrayList<>();

        //split the records into passed and failed
        for(CSVRecord record : records) {
            if(this.checkRecord(record)) {
                failedRecords.add(record);
            }
            else {
                passedRecords.add(record);
            }

        }

        //print a status message
        System.out.println("Passed Records: " + passedRecords.size());
        System.out.println("Failed Records: " + failedRecords.size());

        //process the passedRecords
        this.processPassedRecordsMemory(passedRecords);

        //uncomment below line to use file db
        //this.processPassedRecordsFileDB(passedRecords);

        //process the failed records
        this.writeErrorCSV(failedRecords);
        //write statistics to a log file
        StatWriter.writeStatistics(records.size(), passedRecords.size(), failedRecords.size());

    }

    /**
     * Method to check if a record has missing data
     * @param record CSV Record to test
     * @return true if there is missing data, false otherwise
     */
    private boolean checkRecord(CSVRecord record) {

        return record.get(COLUMN_A).isEmpty() || record.get(COLUMN_B).isEmpty() ||
                record.get(COLUMN_C).isEmpty() || record.get(COLUMN_D).isEmpty() ||
                record.get(COLUMN_E).isEmpty() || record.get(COLUMN_F).isEmpty() ||
                record.get(COLUMN_G).isEmpty() || record.get(COLUMN_H).isEmpty() ||
                record.get(COLUMN_I).isEmpty() || record.get(COLUMN_J).isEmpty();
    }


    /**
     * Method to handle the exporting of failed records to a bad-data-<timestamp>.csv file
     * @param failedRecords and ArrayList of CSVRecords that are missing column data
     */
    private void writeErrorCSV(ArrayList<CSVRecord> failedRecords) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());


        String fileName = String.format("bad-data-%s.csv", sdf.format(timestamp) );

        try{
            FileWriter writer = new FileWriter(fileName);

            CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT);



            for(CSVRecord record : failedRecords) {
                printer.printRecord(
                        record.get(COLUMN_A),
                        record.get(COLUMN_B),
                        record.get(COLUMN_C),
                        record.get(COLUMN_D),
                        record.get(COLUMN_E),
                        record.get(COLUMN_F),
                        record.get(COLUMN_G),
                        record.get(COLUMN_H),
                        record.get(COLUMN_I),
                        record.get(COLUMN_J)
                );
            }

            printer.close();

        } catch (IOException ioException) {
            System.err.println("Error while writing to csv: " + ioException.getMessage());
            ioException.printStackTrace();
        }



    }

    /**
     * Method to process the passed records into a in memory database
     * @param passedRecords
     */
    private void processPassedRecordsMemory(ArrayList<CSVRecord> passedRecords) {

        this.createConnection();
        this.createTableX();
        this.createAndProcessBatchInsert(passedRecords);
        this.closeConnection();
    }

    /**
     * Method to process the passed records into a in file database
     * Assumes that the table has already been created
     * @param passedRecords
     */
    private void processPassedRecordsFileDB(ArrayList<CSVRecord> passedRecords) {

        this.createConnectionFileDB();
        this.createAndProcessBatchInsert(passedRecords);
        this.closeConnection();
    }

    /**
     * Method that will create the batch insert for inserting the data to a db
     * this method is used for in memory and file
     * @param passedRecords
     */
    private void createAndProcessBatchInsert(ArrayList<CSVRecord> passedRecords) {
        try{

            String insert = "INSERT INTO main.X(A, B, C, D, E, F, G, H, I, J) VALUES" +
                    "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            batchStatment = conn.prepareStatement(insert);

            for(CSVRecord record: passedRecords) {
                batchStatment.setString(1, record.get(COLUMN_A));
                batchStatment.setString(2, record.get(COLUMN_B));
                batchStatment.setString(3, record.get(COLUMN_C));
                batchStatment.setString(4, record.get(COLUMN_D));
                batchStatment.setString(5, record.get(COLUMN_E));
                batchStatment.setString(6, record.get(COLUMN_F));
                batchStatment.setString(7, record.get(COLUMN_G));
                batchStatment.setString(8, record.get(COLUMN_H));
                batchStatment.setString(9, record.get(COLUMN_I));
                batchStatment.setString(10, record.get(COLUMN_J));
                batchStatment.addBatch();
            }
            batchStatment.executeBatch();

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void createTableX() {
        try{
            //sql statement to create table X
            String sql = "create table X (A varchar, B varchar, C varchar, D varchar, E varchar, F varchar, G varchar, H varchar, I varchar, J varchar)";

            stmt = conn.createStatement();

            stmt.executeUpdate(sql);

        }
        catch (SQLException sqlException){
            System.err.println("Error creating table");
        }

    }


    /**
     * Method used to open a connection to the database
     */
    private void createConnection() {


        try {

            conn = DriverManager.getConnection("jdbc:sqlite::memory:");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
    }

    /**
     * Method used to open a connection to the database
     */
    private void createConnectionFileDB() {


        try {
            //uses a database in the resource directory for storage
            Class.forName("org.sqlite.JDBC");
            URL resource = MyApp.class.getResource("db/mydb.db");
            String path = new File(resource.toURI()).getAbsolutePath();
            conn = DriverManager.getConnection(String.format("jdbc:sqlite:%s", path));

        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
    }

    /**
     * Method used to close the connection to the database
     */
    private void closeConnection() {
        try{
            if (!conn.isClosed()) {
                conn.close();
            }
        }
        catch(SQLException sqlException) {
            System.err.println("Error Closing Database Connection: " + sqlException.getMessage());
            sqlException.printStackTrace();
        }

    }
}
